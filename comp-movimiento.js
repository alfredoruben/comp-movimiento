{
  const {
    html,
  } = Polymer;
  /**
    `<comp-movimiento>` Description.

    Example:

    ```html
    <comp-movimiento></comp-movimiento>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --comp-movimiento | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CompMovimiento extends Polymer.Element {

    static get is() {
      return 'comp-movimiento';
    }

    static get properties() {
      var cuenta= JSON.parse(sessionStorage.getItem("cuenta"));
      return {
        "_movimientos": {
          type: Array,
          value: null
        },
        "_numeroCuenta":{
          type: String,
          value: cuenta.numero
        },
        "_saldo":{
          type: Number,
          value: cuenta.saldo
        }
      };
    }

    _irCuentas() {
      console.info('comp-movimiento::_irCuentas....')
      this.dispatchEvent(new CustomEvent('ir-cuentas', {composed: true, bubbles: true}));
    } 
    static get template() {
      return html `
      <style include="comp-movimiento-styles comp-movimiento-shared-styles"></style>
      <slot></slot>
      <cells-component-app-header text="Movimientos: Cuenta {{_numeroCuenta}} : {{_saldo}}" icon-left="back">
      </cells-component-app-header>    
      <div style="height: 20px"></div>
      <template is="dom-if" if="{{_movimientos}}" >

        <table class="tabla1">
          <tr>
            <th>ID</th>
            <th>descripcion</th>
            <th>fecha</th>
            <th>valor</th>
          </tr>
          <template is="dom-repeat" items="{{_movimientos}}">
            <tr>
              <td>{{item.id}}</td>
              <td>{{item.descripcion}}</td>
              <td>{{item.fecha}}</td>
              <td>{{item.valor}}</td>
            </tr>
          </template>
        </table>
      </template>




      `;
    }
  }

  customElements.define(CompMovimiento.is, CompMovimiento);
}